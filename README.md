# README #

[![Build Status](https://drone.io/bitbucket.org/tristanbailey/sliminsight/status.png)](https://drone.io/bitbucket.org/tristanbailey/sliminsight/latest)  
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/a0224cae-88c4-46ff-aa67-28c49cb5a1ae/big.png)](https://insight.sensiolabs.com/projects/a0224cae-88c4-46ff-aa67-28c49cb5a1ae)  

### What is this repository for? ###

* Quick summary
This has an api and and app for presenting Insightly as a scrum board for contacts and opportunities

* Version
0.7.0

### How do I get set up? ###

* Summary of set up
* Configuration
Run MongoDB and add a DB of "sliminsight"
On first load call /api/v1/reload

* Dependencies
Need MongoDB
Need php-mongo
Need php 5.5+
Need Insightly account
Optional MySQL

* Database configuration

* How to run tests
Tests are Behat and Mink in ./features/
./vendor/behat/behat/bin/behat

* Deployment instructions

### Contribution guidelines ###

* Writing tests
Tests are Behat and Mink in ./features/ written in Gerkin

* Code review

* Other guidelines
use new array style []
try to have single purpose per file
use functions to set up objects for dependency bootClassname() pass in any $settings you need

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


### To Repo ###

omnipay/stripe
https://github.com/thephpleague/omnipay

