<?php
/**
 * SlimInsight web app
 *
 * PHP version 5
 *
 *  @category SlimInsight
 *  @package Bootstrap
 *  @author Tristan Bailey <tristanbailey@gmail.com>
 *  @license MIT
 *  @version GIT: <git_id>
 *  @link http://holdingbay.co.uk
 */
// TODO : SINGLE ROLE : to run slim app

// DONE : move mongo to middleware or a common call
// TODO : add check to api called
// DONE : add checks to db use (in wrapper?)
// DONE : add error checking before output
// DONE : move from static :: to app SlimInsight - stated with showConData
// DONE : \InsightlyException is not a thing --so ?
// DONE : move to var in setup $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] .
// TODO : prime the system on a new load - check db on load and trigger if no?
// TODO : Make a file of #123 error codes and text for api
// DONE : should I move $app->SlimInsight-> to $SlimInsight = $app->container->get('sliminsight');
// TODO : refactor to make one api call to refresh all, or a cron or cammand bus job to get both. // guzzle x N (2) - register in function or it crawls api

// DONE : move views to twig
// DONE : twig views generate cols.

// TODO : refactor api to not use seq ids


// TODO : stage_id fail
//Type: ErrorException
//Code: 8
//Message: Undefined index: STAGE_ID
//File: /Users/tristanbailey/git/sliminsight/app/routes/app.php
//Line: 94

// test in phpstorm

//if ($loc ===  'debug') {
//    $_SERVER['REQUEST_METHOD'] = 'GET';
//    $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
//    $_SERVER['REQUEST_URI'] = 'opportunities';
//    $_SERVER['SERVER_NAME'] = 'localhost';
//}

define('INC_ROOT', dirname(__DIR__));

// must be included
require INC_ROOT . '/app/init.php';

use Slim\Slim;
use Slim\Views\TwigExtension;
use Tristanbailey\SlimInsight\SlimInsight;
use CompanyNameGenerator\FakerProvider;
use Connect\Connect;


// set up app
/**
 * @var \Slim\Slim $app
 * @contains mixed $render
 * @contains string $mode
 */
$app = new Slim($settings);

$app->configureMode($app->getMode(), function() use ($app) {
    if (class_exists('\Noodlehaus\Config')) {
        $app->config = \Noodlehaus\Config::load(INC_ROOT . "/app/config/{$app->getMode()}.php");
    }
});

// APP setup for TWIG
$view = $app->view();

$view->setTemplatesDirectory($app->config->get('twig.path'));
$view->parserOptions = [
    'debug' => $app->config->get('twig.debug'),
    'cache' => __DIR__ . '/cache'
];
$view->parserDirectory = 'Twig';
$view->parserExtensions = [
    new TwigExtension(),
];

// Also load in a function to the instance of twig in view
/** @var \Slim\Views\Twig $view */
$twig = $view->getInstance();
$function = new Twig_SimpleFunction(
    'probability', function($value) {
    $probability = 0.2;
    if (is_int($value) && $value >= 1) {
        $probability = $value / 100;
        $probability += 0.2;
    }
    return $probability;
}
);
$twig->addFunction($function);

/**
 * Custom error handler
 * @param Exception|array $e
 * @param int $level
 */
$app->error(
    function(\Exception $e, $level = 8) use ($app) {
        //print_r(get_class_methods($e));
        //print_r($e->getTrace());
        $passedLevel = $level;

        // log an error
        $app->log->error($e);

        // unset the e exception for the tpl if in production , so don't show data
        if ($app->mode === 'production') {
            $e = [];
        }

        // ErrorException = undefined key
        if ($e instanceof \MongoConnectionException) {
            return $app->render('500.twig', ['msg' => 'MongoConnectionException', 'e' => $e], 500);
        }
        if ($e instanceof \MongoException) {
            return $app->render('500.twig', ['msg' => 'MongoException', 'e' => $e], 500);
        }
        if ($e instanceof \PDOException) {
            return $app->render('500.twig', ['msg' => 'PDOException', 'e' => $e], 500);
        }
        return $app->render('500.twig', ['msg' => 'Exception', 'e' => $e], 500);
    }
);

// 404 page
$app->notFound(
    function() use ($app) {
        $app->render('404.twig', ['msg' => 'Page Not Found'], 404);
    }
);

// APP set up middleware wrappers

/**
 * Set a server url string for site use
 * @return string
 */
$app->container->set(
    'server', function() use ($app) {
    // TODO : could move to ? $__['HTTP_HOST'] . $__[SCRIPT_NAME];
    if ($app->getMode() === 'development') {
        return $app->config->get('app.url') . $app->config->get('app.loc');
    } else {
        return $app->config->get('app.url');
    }
}
);
/**
     * @return bool|Insightly\Insightly
     */
$app->container->singleton(
    'insightly', function() use ($app, $container) {
    try {
        $i = $container['insightly']($app->config->get('insightly.key'));
        return $i;
    } catch (\Insightly\InsightlyException $e) {
        $app->error($e, \Slim\Log::ERROR);
    }
    return false;

}
);

/**
 * Creates mongo client and makes MongoDB
 * @use \Slim\Slim $app
 * @return bool|MongoDB
 */
$app->container->singleton(
    'mongo', function() use ($app, $container) {
    // test the Mongo is installed and test db exist
    try {
        $mongo = $container['mongo']($app->config->get('mongo.name'));
        return $mongo;
    } catch (\MongoException $e) {
        $app->error($e, \Slim\Log::WARN);
    } catch (\Exception $e) {
        $app->error($e, \Slim\Log::WARN);
    }
    return false;

}
);

// use the factory to create a Faker\Generator instance
$app->container->singleton(
    'faker', function() use ($app) {
    // connect
    $faker = Faker\Factory::create();
    $faker->addProvider(new FakerProvider($faker));
    return $faker;

    /*
    // generate data by accessing properties
    echo $app->faker->name;
    // 'Lucy Cechtelar';
    echo $app->faker->address;
    // "426 Jordy Lodge
    echo $app->faker->text;
    // CompanyNamz
    echo $app->faker->companyName;
    */
}
);


// ? should I pass MongoDB or MongoClient?
// test running the object all though, see if data carried over matters or helps ##TODO : test SlimInsight and ->set vs ->singleton
$app->container->set(
    'SlimInsight', function() use ($app) {
    return new SlimInsight($app->mongo, $app->insightly, $app->faker, $app->config->get('app.fake'));
}
);

/**
 * Eloquent
 * @return bool|Illuminate\Database\Capsule\Manager
 */
$app->container->set(
    'Capsule', function() use ($app, $container) {
    // test the Eloquent is installed and test db exist
    try {
        $capsule = $container['capsule']($app->config->get('db.name'));
        return $capsule;
    } catch (Exception $e) {
        $app->error($e, \Slim\Log::WARN);
    }
    return false;
}
);



/*
 *  =======================================================================
 */

/**
 * a uasort to get the sub STAGE_ORDER to sort the parent
 * @param $arg1
 * @param $arg2
 * @return int
 */
function stageOrder($arg1, $arg2)
{
    if ($arg1['STAGE_ORDER'] > $arg2['STAGE_ORDER']) {
        return 1;
    } elseif ($arg1['STAGE_ORDER'] = $arg2['STAGE_ORDER']) {
        return 0;
    } else {
        return -1;
    }
}

// TODO : should it work offline?
// cache these stages to db
//if ($settings['mode'] == 'production') {
    // set up pipeline stages
    $i = $container['insightly']($app->config->get('insightly.key'));
    $pipelineStages = $i->getPipelineStages();
    
    $pipelineStages = (array) $pipelineStages;
    foreach ($pipelineStages as $stg) {
        $pipeline_stages[$stg->STAGE_NAME] = (array) $stg;
    }

    uasort($pipeline_stages, 'stageOrder');

    $stage = array_merge_recursive($pipeline_stages, $stage);
//}

//var_dump($stages);

/* END stages setup */




if ($settings['mode'] == 'production') {
    /**
     * stats from connect.io visitor logging
     * @param $projectId
     * @param $apiKey
     */
    Connect::initialize($app->config->get('connectio.domain'), $app->config->get('connectio.key'));

    $visit_settings = $app->getDefaultSettings();
    foreach ($visit_settings as $key => $line) {
        $visit_settings[str_replace('.', '__', $key)] = $line;
    }

    $visit = [
        'customer' => [
            'firstName' => 'Tristan',
            'lastName' => 'Bailey',
        ],
        'route' => $app->request->getPathInfo(),
        'server' => $app->config->get('app.url') . $app->config->get('app.loc'),
        'mode' => $app->getMode(),
        'version' => $app->config->get('app.version'),
        'status' => $app->response()->getStatus(),
        'settings' => $app->getDefaultSettings(),
    ];

//var_dump($visit);
    Connect::push('sliminsight', $visit);
}
// END visit logging

// TODO : fake UUID on all api use. IF needed?
// WHEN doing a put, you should use a UUID
// SHOULD the api have more UUID, but prob enough for mongodb
//
// make fake uuid numbers
//use Ramsey\Uuid\Uuid;
//use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
//
//try {
//
//    // Generate a version 3 (name-based and hashed with MD5) UUID object
//    $uuid3 = Uuid::uuid3(Uuid::NAMESPACE_DNS, 'php.net');
//    echo $uuid3->toString() . "\n"; // 11a38b9a-b3da-360f-9353-a5a725514269
//
//    // Generate a version 4 (random) UUID object
//    $uuid4 = Uuid::uuid4();
//    echo $uuid4->toString() . "\n"; // 25769c6c-d34d-4bfe-ba98-e0ee856f3e7a
//
//} catch (UnsatisfiedDependencyException $e) {
//
//    // Some dependency was not met. Either the method cannot be called on a
//    // 32-bit system, or it can, but it relies on Moontoast\Math to be present.
//    echo 'Caught exception: ' . $e->getMessage() . "\n";
//
//}

/**
 * NOW SET UP ROUTES
 */

// this relates to config setup missing or not for tpl layout
if (0 === $stage || is_array($stage) === false) {
    $stage = [];
}

// WEB APP
require INC_ROOT . '/app/routes/app.php';
// APP API
require INC_ROOT . '/app/routes/api.php';
// PRIVATE APP GET remote data
require INC_ROOT . '/app/routes/get.php';

// PUBLIC API
require INC_ROOT . '/app/routes/getapi.php';



// ---- END APP RUN --
$app->run();
