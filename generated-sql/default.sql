
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- users
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users`
(
    `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(20) NOT NULL,
    `first_name` VARCHAR(50),
    `last_name` VARCHAR(50),
    `password` VARCHAR(255) NOT NULL,
    `active` TINYINT(1) DEFAULT 0 NOT NULL,
    `active_hash` VARCHAR(255),
    `recover_hash` VARCHAR(255),
    `remember_identifier` VARCHAR(255),
    `remember_token` VARCHAR(255),
    `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
    `deleted_at` DATETIME DEFAULT '0000-00-00 00:00:00' NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
