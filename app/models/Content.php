<?php
namespace Tristanbailey\SlimInsight;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Content extends Eloquent
{
    protected $table = 'contents';
    protected $guarded = ['id'];
    public $title = [];
    public $alias = [];
    public $introtext = [];
    protected $fillable = [
                'asset_id',
                'title',
                'alias',
                'introtext',
                'fulltext',
                'state',
                'catid',
                'created',
                'created_by',
                'created_by_alias',
                'modified',
                'modified_by',
                'checked_out',
                'checked_out_time',
                'publish_up',
                'publish_down',
                'images',
                'urls',
                'attribs',
                'version',
                'ordering',
                'metakey',
                'metadesc',
                'access',
                'hits',
                'metadata',
                'featured',
                'language',
                'xreference'
                ];
     
}
