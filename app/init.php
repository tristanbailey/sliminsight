<?php
/**
 * SlimInsight web app
 * app set up
 * @version 0.7.0
 */
// TODO : SINGLE ROLE : too handle all app settings and includes

use Slim\Slim;
use Slim\Views\Twig;

use Illuminate\Database\Capsule\Manager as Capsule;

use Insightly\Insightly;



// session set up
session_cache_limiter(false);
session_start();

// catch if the site has just been set up or composer is messed up.
if (file_exists(INC_ROOT . '/vendor/autoload.php')) {
    $loader = include INC_ROOT . '/vendor/autoload.php';
} else {
    throw new \Exception('Autoloader missing, run : composer update');
}

// set up monolog
$logger = new Flynsarmy\SlimMonolog\Log\MonologWriter(
    [
        'handlers' => [
            new Monolog\Handler\StreamHandler('../logs/' . date('Y-m-d') . '.log'),
        ],
    ]
);

// TODO : evaluate two levels of settings need
/**
 * The base Slim $app settings
 * @var array $settings
 */
$settings = [
    'mode' => trim(file_get_contents(INC_ROOT . '/mode.ini')),
    'debug' => true,
    'view' => new Twig(),
    'cookies.secret_key' => 'dc9-Vdsnj;22',
    'templates.path' => '../app/views/',
    'log.writer' => $logger,
    'log.enabled' => true,
    'log.level' => \Slim\Log::DEBUG,
    'version' => '0.7.5',
];

// TODO : test log file set up or in build script
// Log file setup
//if (file_exists('./logs/'.date('Y-m-d').'.log')){
//
//} else {
//    throw new \Exception('Missing Log file');
//}

//
/**
 * insightly pipeline stages
 * array (size=8)
 * 0 =>
 * array (size=5)
 *  'PIPELINE_ID' => int 91304
 *  'STAGE_ID' => int 278132
 *  'STAGE_NAME' => string 'New' (length=3)
 *  'STAGE_ORDER' => int 1
 *  'OWNER_USER_ID' => int 560190
 * ...
 * @var array $stage
 */
$stage = [
    'New' => [
        'COLOUR' => 'rgb(10,200,50)',
    ],
    'Contact' => [
        'COLOUR' => '#ffffff',
    ],
    'Brief' => [
        'COLOUR' => '#1496BB',
    ],
    'Meeting' => [
        'COLOUR' => '#5ca794',
    ],
    'Bid' => [
        'COLOUR' => '#edaa38',
    ],
    'Won' => [
        'COLOUR' => '#f26d21',
    ],
    'Development' => [
        'COLOUR' => '#a7b0b2',
    ],
    'Closed' => [
        'COLOUR' => '#a7b0b2',
    ]
];

/**
 * Container set up for DI objects
 * @var array $container
 */
$container = [];

// TODO : should it work offline? // Httpful\Exception\ConnectionErrorException
#if ($settings['mode'] == 'production') {
// Insightly setup
/**
 * @param string $key
 * @return Insightly
 * @throws Exception
 */
$container['insightly'] = function($key) {
    if (class_exists('Insightly\Insightly')) {
        $i = new Insightly($key);
    } else {
        throw new \Insightly\InsightlyException('Missing Insightly Class', \Slim\Log::ERROR);
    }

    return $i;
};
#} else {
#    $container['insightly'] = function($key) { return true; };
#}

// TODO : test mongo server exists
// TODO : test db fail 'mongo:/does/not/exist'
// TODO : make this better check //is_a($m, 'MongoClient') &&
/**
 * MongoDB setup
 * @var array $container
 * @param string $database
 * @param string $serverUri
 * @return MongoDB
 * @throws Exception
 * @throws MongoConnectionException
 * @internal param null|string $server
 */
$container['mongo'] = function($database, $serverUri = null) {
    /**
     * @var string $serverUri
     */
    $server = ((string) $serverUri !== null) ? $serverUri : 'mongodb://localhost:27017';
    $options = ['connect' => true];

    // test the Mongo is installed
    if (class_exists('MongoClient')) {
        try {
            // connect
            /**
             * @var string $server
             * @var array $options
             */
            $m = new MongoClient($server, $options);

        } catch (\MongoConnectionException $e) {
            throw new \MongoConnectionException('Mongo Connection Error', \Slim\Log::FATAL);
        } catch (\Exception $e) {
            throw new \Exception('Mongo Error', \Slim\Log::FATAL);
        }
    } else {
        throw new \Exception('Missing MongoClient Class built into PHP', \Slim\Log::FATAL);
    }

    // get available db from server
    $databaseAvailable = $m->listDBs();

    // put the names in the array to test
    foreach ($databaseAvailable['databases'] as $var) {
        $databaseAvailable[] = $var['name'];
    }

    // optional that checks if db there
    if (!in_array($database, $databaseAvailable, true)) {
        // error but no log possible as loader
        // $app->log->error('Missing MongoDB database. Expecting: ' . $database);
        echo 'Missing MongoDB database. Expecting: ' . $database;
    }

    $mongo = $m->selectDB($database);
    if (!isset($mongo)) {
        throw new \Exception('Missing MongoDB database. Expecting: ' . $database, \Slim\Log::ERROR);
    }
    /**
     * @var MongoDB $mongo
     */
    return $mongo;
};

/**
 * boot Eloquent Manager Illuminate\Database\Capsule\Manager
 * DB set up
 * $capsule = $container['capsule']($settings['mysql']);
 *
 * @param array $settings
 * @return Capsule
 * @throws Exception
 */
$container['capsule'] = function(array $settings) {
    $capsule = new Capsule();
    $capsule->addConnection($settings);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    if ($capsule->getConnection() === false) {
        throw new Exception('Eloquent Failed', \Slim\Log::FATAL);
    }
    return $capsule;
};

// include classes

Slim::registerAutoloader();
