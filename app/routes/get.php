<?php
/**
     * Created by PhpStorm.
     * User: tristanbailey
     * Date: 01/08/2015
     * Time: 04:33
     */

// -------------------- GET: SOURCE DATA ------------------------------------------------------------------------------
//
// DONE : add to class so the route calls method

// update db for opportunities
$app->get(
    '/get/db/opportunities', function() use ($app) {
    $SlimInsight = $app->container->get('SlimInsight');

    // TODO : test or try catch get data
    // get the refresh from the obj
    $SlimInsight->opportunities->refresh();
    $output = $SlimInsight->opportunities->output();

    // get current path for the route
    if ($currentRoute = $app->request->getPathInfo()) {
        $output['route'] = $currentRoute;
    }

    $app->contentType('application/json');
    $app->render('json.twig', ['data' => $output], $output['response']);
    }
);

// TODO add to class
// update db for contacts
$app->get(
    '/get/db/contacts', function() use ($app) {
    $SlimInsight = $app->container->get('SlimInsight');

    // TODO : test or try catch get data
    // get the refresh from the obj
    $SlimInsight->contacts->refresh();
    $output = $SlimInsight->contacts->output();

    // get current path for the route
    if ($currentRoute = $app->request->getPathInfo()) {
        $output['route'] = $currentRoute;
    }

    $app->contentType('application/json');
    $app->render('json.twig', ['data' => $output], $output['response']);
    }
);
