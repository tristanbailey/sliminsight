<?php
/**
     * Created by PhpStorm.
     * User: tristanbailey
     * Date: 01/08/2015
     * Time: 04:39
     */

use GuzzleHttp\Client as GuzzleClient;

/*
// TODO tests with exceptions round the route
// use the api to dog food and return filtered data for use.
// either to test using the api or to share api to users filtered
try {
// TODO : changed /api/v1/get/contacts ? or POST GET type thing?
// TODO : handle errors on guzzle
*/
// api get call
$app->get(
    '/get/api/contacts', function() use ($app) {
    $data = [];
    $client = new GuzzleClient(
        [
            // Base URI is used with relative requests
            'base_uri' => $app->server,
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]
    );
    $loc = $app->config->get('app.loc');

    try {
        $response = $client->get($loc . '/api/v1/contacts', ['debug' => $app->config('debug')]); // /130849375

        $output = (string) $response->getBody();

        if ($output != '') {
            /**
             * @var array $output
             */
            $output = json_decode($output);
            if (is_object($output) && is_callable($output->data)) {
                $data = $output->data;
            }
        }

        // filter list as array not SlimInsight
        $allowed = ['CONTACT_ID', 'FIRST_NAME', 'LAST_NAME', 'DATE_CREATED_UTC', 'DATE_UPDATED_UTC', 'CONTACTINFOS', 'TAGS', 'LINKS'];
        foreach ($data as $key => $d) {
            $d = (array) $d;
            $dataFiltered[$key] = array_intersect_key($d, array_flip($allowed));
        }
        $data = (isset($dataFiltered)) ? $dataFiltered : [];

        $response_code = (count($data) >= 1) ? 200 : 404;

    } catch (GuzzleHttp\Exception\ClientException $e) {
        $response_code = $e->getCode(); // use the response code from the $e

        // its an api so just log the error not throw to screen
        if ($app->getMode() === 'development') {
            $app->log->error($e);
        } else {
            $app->error($e, \Slim\Log::ERROR);
        }
    }

    $output = [
        'response' => $response_code,
        'route' => '/get/api/contacts',
        'count' => count($data),
        'data' => $data
    ];

    // output
    $app->contentType('application/json');
    $app->render('json.twig', ['data' => $output], $response_code);
}
);

$app->get(
    '/get/api/contacts/:contact_id', function($contact_id) use ($app) {
    $data = [];
    $client = new GuzzleClient(
        [
            // Base URI is used with relative requests
            'base_uri' => $app->server,
            // You can set any number of default request options.
            'timeout'  => 2.0,
        ]
    );
    $loc = $app->config->get('app.loc');
    /**
     * @var null|int $contact_id
     */
    $contact_id = ((int) $contact_id >= 1) ? $contact_id : null;

    try {
        //$response = $client->get('/get/contacts', ['debug' => true]);
        $response = $client->get($loc . '/api/v1/contacts/' . $contact_id, ['debug' => $app->config('debug')]); // /130849375

        $output = (string) $response->getBody();

        if ($output != '') {
            /**
             * @var array $output
             */
            $output = json_decode($output);
            if (is_object($output) && is_callable($output->data)) {
                $data = $output->data;
            }
        }

        // filter list as array not SlimInsight
        $allowed = ['CONTACT_ID', 'FIRST_NAME', 'LAST_NAME', 'DATE_CREATED_UTC', 'DATE_UPDATED_UTC', 'CONTACTINFOS', 'TAGS', 'LINKS'];
        foreach ($data as $key => $d) {
            $d = (array) $d;
            $dataFiltered[$key] = array_intersect_key($d, array_flip($allowed));
        }
        $data = (isset($dataFiltered)) ? $dataFiltered : [];

        $response_code = (count($data) >= 1) ? 200 : 404;

    } catch (GuzzleHttp\Exception\ClientException $e) {
        $response_code = $e->getCode(); // use the response code from the $e

        // its an api so just log the error not throw to screen
        if ($app->getMode() === 'development') {
            $app->log->error($e);
        } else {
            $app->error($e, \Slim\Log::ERROR);
        }
    }

    $output = [
        'response' => $response_code,
        'route' => '/get/api/contacts',
        'count' => count($data),
        'data' => $data
    ];

    // output
    $app->contentType('application/json');
    $app->render('json.twig', ['data' => $output], $response_code);
}
);
