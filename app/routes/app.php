<?php
/**
 * Created by PhpStorm.
 * User: tristanbailey
 * Date: 01/08/2015
 * Time: 04:36
 */
// TODO : pass TO all VIEWS if it is a FAKE view to change the layout?

// -------------------- APP -------------

// contacts col page
$app->get(
    '/contacts', function() use ($app) {
    $SlimInsight = $app->container->get('SlimInsight');
    $SlimInsight->contacts->get();
    $output = $SlimInsight->contacts->output();

    $currentRoute = $app->router()->getCurrentRoute()->getName();
    $output['route'] = $currentRoute;

    // convert to category nested, preload the order
    $category_data = [
        'New Leads' => [],
        'Warm Leads' => [],
        'Hot Leads' => [],
        'Current Clients' => [],
        'OTHER' => [],
    ];
    $skip = [];
    foreach ($output['data'] as $document) {
        if (strtotime('2014-04-01') >= strtotime($document['DATE_CREATED_UTC'])) {
            // skip
            $skip['SKIP'][] = $document;
        } else {

            // TODO : think about that if a tag is found it will not look for others or // in_array() for each one as could be less loop?
            // Put the contact documents in to lead categories
            // Only put in one category not one for each
            foreach ($document['TAGS'] as $tag) {
                if ($tag['TAG_NAME'] === 'New Leads') {
                    $category_data['New Leads'][] = $document;
                    break;
                } elseif ($tag['TAG_NAME'] === 'Warm Leads') {
                    $category_data['Warm Leads'][] = $document;
                    break;
                } elseif ($tag['TAG_NAME'] === 'Hot Leads') {
                    $category_data['Hot Leads'][] = $document;
                    break;
                } elseif ($tag['TAG_NAME'] === 'Current Clients' || $tag['TAG_NAME'] === 'Past Clients') {
                    $category_data['Current Clients'][] = $document;
                    break;
                } else {
                    $category_data['OTHER'][] = $document;
                }
            }

        }
    }
    $output['data'] = $category_data;

    $app->render('contacts.twig', $output, $output['response']);

}
)->name('contacts');

// opportunities col page
$app->get(
    '/opportunities', function() use ($app, $stage) {
    $cur = $app->router()->getCurrentRoute()->getName();

    $SlimInsight = $app->container->get('SlimInsight');
    $SlimInsight->opportunities->get();
    $output = $SlimInsight->opportunities->output();

    // convert to category nested, preload the order
    $category_data = [
        'OPEN' => [],
        'Bid' => [],
        'Won' => [],
        'Development' => [],
        'PROD' => [],
    ];
    $skip = [];
    foreach ($output['data'] as $document) {
        // var_dump($document['STAGE_ID']);
        // echo $document['STAGE_ID'] . '<br />'; ##TB test
        // 1985535 is closed
        // skip in category or created is before april 2014
        if ($document['CATEGORY_ID'] == '1985535' || strtotime('2014-04-01') >= strtotime($document['DATE_CREATED_UTC'])) {
            // skip
            $skip['SKIP'][] = $document;
        } else {
            if ($document['STAGE_ID'] === $stage['Bid']['STAGE_ID']) {
                $category_data['Bid'][] = $document;
            } elseif ($document['STAGE_ID'] === $stage['Won']['STAGE_ID']) {
                $category_data['Won'][] = $document;
            } elseif ($document['STAGE_ID'] === $stage['Development']['STAGE_ID']) {
                $category_data['Development'][] = $document;
            } elseif ($document['STAGE_ID'] === $stage['Closed']['STAGE_ID']) {
                $category_data['PROD'][] = $document;
            } elseif (array_key_exists('STAGE_ID', $document) && !empty($document['STAGE_ID'])) {
                $category_data['OPEN'][] = $document;
            } else {
                echo 'none : ' . $document['NAME'];
                $category_data['NONE'][] = $document;
            }
        }
    }
    $output['data'] = $category_data;

    // add stages array
    $output['stage'] = $stage;
    //foreach ($category_data as $key => $d)
    //{
    //    echo $key . ' count = ' . count($d) . PHP_EOL;
    //}
    //print_r($category_data);

    $output['cur'] = $cur;
    $app->render('opportunities.twig', $output, $output['response']);
}
)->name('opportunities');

// opportunities month page
$app->get(
    '/opportunities/month', function() use ($app, $stage) {
    $SlimInsight = $app->container->get('SlimInsight');
    $SlimInsight->opportunities->get();
    $output = $SlimInsight->opportunities->output();

    // column setup in format '06jun'
    $col1 = strtolower(date('mM', strtotime('-2 months')));
    $col2 = strtolower(date('mM', strtotime('-1 months')));
    $col3 = strtolower(date('mM'));
    $col4 = strtolower(date('mM', strtotime('+1 months')));

    // convert to category nested, preload the order
    $category_data = [
        $col1 => [],
        $col2 => [],
        $col3 => [],
        $col4 => [],
    ];
    $skip = [];
    foreach ($output['data'] as $document) {
        if (strtotime('2014-04-01') >= strtotime($document['DATE_CREATED_UTC'])) {
            // skip
            $skip['SKIP'][] = $document;
        } else {

            // Put the opportunity documents in to lead categories
            // Only put in one category not one for each
            foreach ($document['TAGS'] as $tag) {
                if ($tag['TAG_NAME'] == $col1) {
                    $category_data[$col1][] = $document;
                    break;
                } elseif ($tag['TAG_NAME'] == $col2) {
                    $category_data[$col2][] = $document;
                    break;
                } elseif ($tag['TAG_NAME'] == $col3) {
                    $category_data[$col3][] = $document;
                    break;
                } elseif ($tag['TAG_NAME'] == $col4) {
                    $category_data[$col4][] = $document;
                    break;
                }
            }
        }

    }
    $output['data'] = $category_data;

    // add stages array
    $output['stage'] = $stage;

    $app->render('opportunities_month.twig', $output, $output['response']);
}
)->name('opportunities-month');
