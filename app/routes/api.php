<?php
/**
 * Routes for API
 * User: tristanbailey
 * Date: 01/08/2015
 * Time: 04:29
 */

// TODO : fix the way routes show for different paths

// -------------------- API ------------------------------------------------------------------------------

$app->group(
    '/api', function() use ($app) {

    $app->group(
        '/v1', function() use ($app) {

        // describe the api
        $app->get(
            '/', function() use ($app) {
            $output = [
                'response' => '200',
                'name' => 'SlimInsight',
                'url' => $app->server . '/api/v1',
                'routes' => [
                    $app->server . '/api/v1' . '/contacts',
                    $app->server . '/api/v1' . '/contacts/<id>',
                    $app->server . '/api/v1' . '/opportunities',
                    $app->server . '/api/v1' . '/opportunities/<id>'
                ],
                'authentication' => ''
            ];
            $app->contentType('application/json');
            $app->render('json.twig', ['data' => $output], $output['response']);
        }
        );

        // get all contacts
        $app->get(
            '/contacts', function() use ($app) {
            $SlimInsight = $app->container->get('SlimInsight');
            $output = $SlimInsight->contacts->get();

            // get current path for the route
            if ($currentRoute = $app->request->getPathInfo()) {
                $output['route'] = $currentRoute;
            }

            $app->contentType('application/json');
            $app->render('json.twig', ['data' => $output], $output['response']);
        }
        )->name('get-contacts');

        // get contact with id
        $app->get(
            '/contacts/:contact_id', function($contact_id) use ($app) {
            $SlimInsight = $app->container->get('SlimInsight');
            $output = $SlimInsight->contacts->get((int) $contact_id);

            // get current path for the route
            if ($currentRoute = $app->request->getPathInfo()) {
                $output['route'] = $currentRoute;
            }

            $app->contentType('application/json');
            $app->render('json.twig', ['data' => $output], $output['response']);
            // OR #TODO : do I need a data not found
            // $app->notFound();
        }
        )->name('get-contacts-id');

        // get all opportunities
        $app->get(
            '/opportunities', function() use ($app) {
            $SlimInsight = $app->container->get('SlimInsight');
            $output = $SlimInsight->opportunities->get();

            // get current path for the route
            if ($currentRoute = $app->request->getPathInfo()) {
                $output['route'] = $currentRoute;
            }

            $app->contentType('application/json');
            $app->render('json.twig', ['data' => $output], $output['response']);
        }
        )->name('get-opportunities');

        // get opportunities with id
        $app->get(
            '/opportunities/:opportunity_id', function($opportunity_id) use ($app) {
            $SlimInsight = $app->container->get('SlimInsight');
            $output = $SlimInsight->opportunities->get((int) $opportunity_id);

            // get current path for the route
            if ($currentRoute = $app->request->getPathInfo()) {
                $output['route'] = $currentRoute;
            }

            $app->contentType('application/json');
            $app->render('json.twig', ['data' => $output], $output['response']);
        }
        )->name('get-opportunities-id');

        // api get call for all routes async
        // * these are routes of the app, not of the object
        $app->get(
            '/refresh', function() use ($app) {
            $data = [];
            // a base path for diff servers
            $loc = $app->config->get('app.loc');
            $client = new \GuzzleHttp\Client(
                [
                    'base_uri' => $app->config->get('app.url'),
                    'timeout' => 9.0,
                ]
            );
            $fail = false;
            // Initiate each request but do not block
            $promises = [
                'opportunities' => $client->getAsync($loc . '/get/db/opportunities'),
                'contacts'   => $client->getAsync($loc . '/get/db/contacts'),
            ];

            try {
                // Wait on all of the requests to complete.
                $results = \GuzzleHttp\Promise\unwrap($promises);

                foreach ($results as $key => $response) {
                    if ($response->getStatusCode() != 200) {
                        // false - call didn't work
                        $fail = $key;
                    }
                    $output = (string) $response->getBody();
                    /**
                     * @var array $output
                     */
                    $output = json_decode($output);

                    if ($output->count <= 0) {
                        // false - got no data
                        $fail = $key;
                    } else {
                        $data[$key] = $output;
                    }
                }
            }
            catch (\Exception $e)
            {
//                 echo $e->getCode();
//                 print_r($e->getTrace());
//                 $app->error($e, \Slim\Log::ERROR);
                $fail = 404;
            }

            $response_code = ($fail === false) ? 200 : 404;

            $output = [
                'response' => $response_code,
                'route' => '/v1/api/refresh',
                'count' => count($data),
                'data' => $data,
            ];

            // output
            $app->contentType('application/json');
            $app->render('json.twig', ['data' => $output], $response_code);
            }
        )->name('refresh');
        }
    ); // \v1

}
); // \api
