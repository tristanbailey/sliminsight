<?php
/**
 * FakeCollectionOpportunity Class
 * change the data for CollectionOpportunity Class
 *
 * PHP version 5
 *
 * @category Tristanbailey\SlimInsight
 * @package  Opportunity
 * @author   Tristan Bailey <tristanbailey@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @version  GIT: <git_id>
 * @link     http://holdingbay.co.uk
 */

// DONE : finish set up fake
// TODO : Should it just be output that styles fake?
// TODO : #Phase2 make possible to fake all the data and not use parent.

// TODO : move the output to output and return data from the calls. // NO return true / false from actions and output does return.
// TODO : #Phase2 do you now only need output() in here?

namespace Tristanbailey\SlimInsight\Opportunity;

use Faker\Generator as Faker;

/**
 * Class FakeCollectionOpportunity
 * @property \Tristanbailey\SlimInsight\Opportunity\CollectionOpportunity opportunities
 */
class FakeCollectionOpportunity extends CollectionOpportunity
{
    /**
     * @var Faker
     */
    private $faker;
    /**
     * @var bool
     */
    private $isFake = true;
    /**
     * @var array
     */
    private $data = [];

    /**
     * @param \MongoDB $db
     * @param \Insightly\Insightly $insightly
     * @param Faker $faker
     * @param bool $isFake
     * @throws \Exception
     */
    public function __construct(\MongoDB $db, \Insightly\Insightly $insightly, Faker $faker, $isFake = true)
    {
        parent::__construct($db, $insightly);
        $this->isFake = $isFake;

        // TODO : do not need to test if fake, as will be by default // Can you set it to not fake pass through?
        // need faker for this
//        if ($isFake === true && is_object($faker) && get_class($faker) === 'Faker\Generator') {
        if (is_object($faker) && get_class($faker) === 'Faker\Generator') {
//            $this->isFake = $isFake;
            $this->faker = $faker;
        }

        // TODO  : might not reach so not needed?
        if (is_null($this->faker)) {
            throw \Exception('Object requires Faker');
        }
    }

    /**
     * Return an $output of all on 1 Opportunity from the DB
     * Should pass in an int but force that from the outside? Or force correct? << is hard to get all and 1 if Type here
     * @param bool|int $Opportunity_id
     * @return array
     */
    public function get($opportunity_id = false)
    {
        /**
         * @var array $data
         */
        $data = [];
        /**
         * @var array $dataClean
         */
        $dataClean = [];
        try {
            $data = parent::get();
        } catch (\Exception $e) {
            echo $e->getMessage() . ' ' . $e->getFile();
        }
        $data = $data['data'];

        $this->opportunities = $data;
//        foreach ($data as $key => $document) {
//            $document['OPPORTUNITY_NAME'] = ($r = mt_rand(0, 10) >= 5) ? $this->faker->companyName : $this->faker->company;
//            $document['BID_AMOUNT'] = ($r = mt_rand(0, 10) >= 8) ? $this->faker->numberBetween(300, 9000) : $this->faker->numberBetween(300, 6000);
//            $document['FORECAST_CLOSE_DATE'] = $this->faker->dateTimeBetween('-1 years', '+9 months')->format('Y-m-d H:i:s');
//            $document['OPPORTUNITY_DETAILS'] = $this->faker->realText(300);
//            // reset the keys to ids so can be iterated
//            $dataCleaned[] = $document;
//        }
//        $this->opportunities = $dataCleaned;

        /**
         * @var array $output
         */
//        $output = $this->output();
//        return $output;
        return true;

    }

    /**
     * @param string $tag
     * @return bool|string
     */
    public function getTag($tag)
    {
        $dataCleaned = [];

        parent::getTag($tag);
        $data = $this->opportunities;

        // TODO : check return format
        $this->opportunities = $data;
//        foreach ($data as $key => $document) {
//            $document['OPPORTUNITY_NAME'] = ($r = mt_rand(0, 10) >= 5) ? $this->faker->companyName : $this->faker->company;
//            $document['BID_AMOUNT'] = ($r = mt_rand(0, 10) >= 8) ? $this->faker->numberBetween(300, 9000) : $this->faker->numberBetween(300, 6000);
//            $document['FORECAST_CLOSE_DATE'] = $this->faker->dateTimeBetween('-1 years', '+9 months')->format('Y-m-d H:i:s');
//            $document['OPPORTUNITY_DETAILS'] = $this->faker->realText(300);
//            // reset the keys to ids so can be iterated
//            $dataCleaned[] = $document;
//        }
//        $this->opportunities = $dataCleaned;
        /**
         * @var array $output
         */
//        $output = $this->output();
//        return $output;
        return true;
    }

    public function output($data = null, $response_code = null)
    {
        $dataCleaned = [];
        $output = parent::output($data, $response_code);

        foreach ($output['data'] as $key => $document) {
            $document['OPPORTUNITY_NAME'] = ($r = mt_rand(0, 10) >= 5) ? $this->faker->companyName : $this->faker->company;
            $document['BID_AMOUNT'] = ($r = mt_rand(0, 10) >= 8) ? $this->faker->numberBetween(300, 9000) : $this->faker->numberBetween(300, 6000);
            $document['FORECAST_CLOSE_DATE'] = $this->faker->dateTimeBetween('-1 years', '+9 months')->format('Y-m-d H:i:s');
            $document['OPPORTUNITY_DETAILS'] = $this->faker->realText(300);
            // reset the keys to ids so can be iterated
            $dataCleaned[] = $document;
        }
        $output['data'] = $dataCleaned;

        return $output;
    }


}
/* /FakeCollectionOpportunity */
