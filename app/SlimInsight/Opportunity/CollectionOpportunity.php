<?php
/**
 * This file is part of the tristanbailey/sliminsight app
 *
 * CollectionOpportunity Class
 * to look up and generate data from the Insightly api
 *
 * PHP version 5
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Tristan Bailey <tristanbailey@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://holdingbay.co.uk/projects/sliminsight/ Documentation
 * @link https://bitbucket.org/tristanbailey/sliminsight GitHub
 */

// TODO : clean down now abstracted

namespace Tristanbailey\SlimInsight\Opportunity;

use Tristanbailey\SlimInsight\CollectionInterface;
//use Faker\Generator as Faker;

/**
 * Class CollectionOpportunity
 * @property \Tristanbailey\SlimInsight\Opportunity\CollectionOpportunity opportunities
 * @property array data
 */
class CollectionOpportunity implements \Iterator, CollectionInterface
{

    private $position = 0;
    private $iteration = 50;
    private $data = [];
    private $db;
    private $insightly;
//    private $faker;
//    private $isFake = false;

    /**
     * @param \MongoDB $db
     * @param \Insightly\Insightly $insightly
//     * @param Faker $faker
//     * @param bool $isFake
     * @throws \Exception
     */
    public function __construct(\MongoDB $db, \Insightly\Insightly $insightly, Faker $faker = null, $isFake = false)
    {
        $this->position = 0;
        $this->data = []; // opportunities
        $this->db = $db;
        $this->insightly = $insightly;

//        if ($isFake === true && is_object($faker) && get_class($faker) === 'Faker\Generator') {
//            $this->isFake = $isFake;
//            $this->faker = $faker;
//        }


        if (!isset($this->db) && !isset($this->insightly)) {
            // TODO : make this better or remove
            throw new \Exception('Could not set up SlimOpportunity');
        }

    }

    /**
     * magic method to allow (string) $object to to return something not fail
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }

    /**
     * Getter
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        if ($key === 'data') {
            return $this->data;
        }
        return $this->data[$key];
    }

    /**
     * Setter
     * @param $key
     * @param $value
     * @return mixed
     */
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
        return $this->data[$key];
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->opportunities[$this->position];
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return integer scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->opportunities[$this->position]);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return $this->data;
        // TODO : should json encode return more vars?
    }

    /**
     * Alias for built in
     * @param bool|int $contact_id
     * @return array
     */
    public function get($contact_id = false)
    {
        $output = $this->getOpportunity($contact_id);
        return $output;
    }

    // TODO : this is a mix of db and obj setup
    // Get from object if set
    // or just use $this->data is set else get?
    // if not set OR forced then get from db

    /**
     * Get the obj data array from db
     * @param int|bool $opportunity_id
     * @return array
     */
    public function getOpportunity($opportunity_id = false)
    {
        $dataCleaned = [];
        /**
         * @var array $data
         */
        $data = [];
        // if its a non number then return a fail
        // A not false + B less 0 OR + is not int
        if ($opportunity_id !== false && ($opportunity_id < 0 || is_int($opportunity_id) === false)) {
            return $this->output($data, 400);
        }

        // TODO : what if db fails
        /**
         * select your collection
         * @var \MongoCollection $collection
         */
        $collection = $this->db->opportunities;

        // find one opportunity
        if ($opportunity_id !== false) {
            $opportunity_id = (int) $opportunity_id;
            /**
             * @var \MongoCursor $cursor
             */
            $cursor = $collection->find(['OPPORTUNITY_ID' => $opportunity_id]);
        } else {
            // find X in the collection
            /**
             * @var \MongoCursor $cursor
             */
            $cursor = $collection->find()->sort(['OPPORTUNITY_STATE' => 1, 'FORECAST_CLOSE_DATE' => 1]);
        }

        // TODO : what if $cursor fails
        /**
         * @var \MongoCursor $data
         */
        $data = iterator_to_array($cursor);

//        foreach ($data as $key => $document)
//        {
//            // reset the keys to ids so can be iterated
//            $dataCleaned[] = $document;
//        }
//        // Store data in the object
//        $this->opportunities = $dataCleaned;
        $this->opportunities = $data;

        /**
         * @var array $output
         */
//        $output = $this->output($this->opportunities);
        return true;
    }

    /**
     * Got to the api and get new data
     * Save data to local nosql
     * requires db and insightly
     * @return array|\Exception
     */
    public function refresh()
    {
        $data = [];
        $mongo = $this->db;
        $i = $this->insightly;

        // call api and get results
        $opportunities = $i->getOpportunities();

        // TODO : if (false) {} // if no data dont save.

        // connect
        // TODO : check there is collection and can save
        if ($mongo->selectCollection('opportunities') === false) {
            return \Exception('Mongo Collection Missing: opportunities', \Slim\Log::CRITICAL);
        }

        // drop old data
        $mongo->opportunities->remove();
        $this->opportunities = [];

        // pass cursor over for use
        /**
         * @var \MongoCollection $collection
         */
        $collection = $mongo->opportunities;

        // TODO : check there is save or do all as one insert?
        // INSERT into mongo - less data inserts // TEST
        // put data in mongo
        if (isset($opportunities)) {
            $collection->batchInsert((array) $opportunities);
        } else {
            return false;
        }

// -- can I do this without call?
        //$this->opportunities = $mongo->opportunities;
//
        /**
         * put mongo data in obj
         * @var \MongoCollection $collection
         */
        $cursor = $collection->find();
        $this->opportunities = iterator_to_array($cursor);

//        foreach ($this->opportunities as $doc) {
//            // save a record of ids for output
//            $data[] = $doc['OPPORTUNITY_ID'];
//        }

        /**
         * @var array $output
         */
//        $output = $this->output($data);
        return true;
    }

    /**
     *
     */
    public function getTag($tag)
    {
        $dataCleaned = [];

        // TODO : api or mongodb search local.
        // the path for this is "/v2.1/Opportunities?tag=$tag";
        $opportunities = $this->insightly->getOpportunities(['tag' => $tag]);

        // select your collection
//        $collection = $this->db->opportunities;

        $data = (array) $opportunities;

        // TODO : should remove the clean --> output
        foreach ($data as $key => $document)
        {
            // TODO : if there is no data in iterator then data will not get set // maybe set fore to this->data?
            // reset the keys to ids so can be iterated
            $dataCleaned[] = $document;
        }
        $this->opportunities = $dataCleaned;

        /**
         * @var array $output
         */
        $output = $this->output();
//        return $output;
        return true;
    }

    /**
     * Prep the output array, same for all formats
     * @param array $data
     * @param int $response_code
     * @return array
     */
    public function output($data = null, $response_code = null)
    {
        // TODO : output should paginate return data
        // TODO : should data test and "return false" to fail output // but api would just be 404
        // if pass no data then use the obj
        if ($data === null) {
            $data = $this->opportunities;
        }
        if (!is_array($data)) {
            $data = [];
        }
        $dataCleaned = [];
        $output = [];

        $response_code = ($response_code !== null || count($data) >= 1) ? 200 : 400;

        /**
         * @var int $count
         */
        $count = count($data);

        foreach ($data as $doc) {
            // reset the ids
            $dataCleaned[] = $doc;
        }
        $data = $dataCleaned;

        /**
         * @var array $output
         */
        $output = [
            'response' => $response_code,
            'route' => '',
            'count' => $count,
            'data' => $data
        ];
        return $output;
    }

}
/* /CollectionOpportunity */
