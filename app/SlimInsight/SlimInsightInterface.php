<?php
/**
 * This file is part of the tristanbailey/sliminsight app
 *
 * SlimInsightInterface Class
 * to look up and generate data from the Insightly api
 *
 * PHP version 5
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Tristan Bailey <tristanbailey@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://holdingbay.co.uk/projects/sliminsight/ Documentation
 * @link https://bitbucket.org/tristanbailey/sliminsight GitHub
 */

namespace Tristanbailey\SlimInsight;


interface SlimInsightInterface extends \JsonSerializable
{

    /**
     * @return array
     */
    public function output();

    /**
     * @return array
     */
//    public function refreshOpportunities();

    /**
     * @return array
     */
//    public function refreshContacts();

    /**
     * @return array
     */
//    public function getOpportunitiesStatic();

    /**
     * @return array
     */
//    public function getOpportunities();

    /**
     * @return array
     */
//    public function getOpportunitiesMonth();

    /**
     * @return array
     */
//    public function getContacts();

}
