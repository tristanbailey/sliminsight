<?php
/**
 * This file is part of the tristanbailey/sliminsight app
 *
 * SlimInsight Class
 * to look up and generate data from the Insightly api
 *
 * PHP version 5
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Tristan Bailey <tristanbailey@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://holdingbay.co.uk/projects/sliminsight/ Documentation
 * @link https://bitbucket.org/tristanbailey/sliminsight GitHub
 */
// TODO : caching?
// TODO : refresh data only go to db on cache call / timeout? //PART DONE
// TODO : refactor to have a generic get data call. Even an OBJ to do that? +1 //PART DONE

// TODO : should children be singular or group (group now unless I want to edit)
// TODO : should have a get all data call
// TODO : should have a refresh all data call

namespace Tristanbailey\SlimInsight;

use Tristanbailey\SlimInsight\CollectionInterface;
use Tristanbailey\SlimInsight\SlimInsightInterface;
use Tristanbailey\SlimInsight\Contact\CollectionContact;
use Tristanbailey\SlimInsight\Contact\FakeCollectionContact;
use Tristanbailey\SlimInsight\Opportunity\CollectionOpportunity;
use Tristanbailey\SlimInsight\Opportunity\FakeCollectionOpportunity;

/**
 * Class SlimInsight
 * @property \Tristanbailey\SlimInsight\Contact\CollectionContact contacts
 * @property \Tristanbailey\SlimInsight\Opportunity\CollectionOpportunity opportunities
 */
class SlimInsight implements SlimInsightInterface
{

    private $position = 0;
//    private $iteration = 50;
    private $data = [];
    private $db;
    private $insightly;
    private $faker;
    private $isFake = false;

    /**
     * @param \MongoDB $db
     * @param \Insightly\Insightly $insightly
     * @param \Faker\Generator $faker
     * @param bool $isFake
     * @throws \Exception
     */
    public function __construct(\MongoDB $db, \Insightly\Insightly $insightly, \Faker\Generator $faker = null, $isFake = false)
    {
        $this->position = 0;
        // TODO : do i need to set this up inside this parent? // do i need this db? could use it // also output could be in here?
        // TODO : can get all inside objects, but when do i need all on the page?
        $this->db = $db;
        $this->insightly = $insightly;

        if ($isFake === true && is_object($faker) && get_class($faker) === 'Faker\Generator') {
            $this->isFake = $isFake;
            $this->faker = $faker;
        }

        // DONE : refactor that you can add in any object bellow into this->data
        if ($this->isFake === true) {
            $this->set('contacts', new FakeCollectionContact($this->db, $insightly, $this->faker, $this->isFake));
            $this->set('opportunities', new FakeCollectionOpportunity($this->db, $this->insightly, $this->faker, $this->isFake));
        } else {
            $this->set('contacts', new CollectionContact($db, $insightly));
            $this->set('opportunities', new CollectionOpportunity($db, $insightly));
        }

        if (!isset($this->db) && !isset($this->insightly)) {
            // TODO : make this better or remove
            throw new \Exception('Could not set up SlimInsight');
        }

    }

    /**
     * magic method to allow (string) $object to to return something not fail
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }

    /**
     * Setter
     * @param $key
     * @param $value
     * @return mixed
     */
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
        return $this->data[$key];
    }

    /**
     * Getter
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->data[$key];
    }

    // TODO : #Phase2 consider output() refactor like children
    /**
     * Prep the output array, same for all formats
     * @param array|bool $data
     * @param int        $response_code
     * @param bool       $route
     * @return array
     */
    public function output($data = [], $response_code = 200, $route = false)
    {
        /**
         * @var int $count
         */
        $count = count($data);

        /**
         * @var array $output
         */
        $output = [
            'response' => $response_code,
            'route' => $route,
            'count' => $count,
            'data' => $data
        ];
        return $output;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return $this->data;
        // TODO : should json encode return more data?
    }

    // get() / getAll()
    public function getAll($obj = null)
    {
        $this->get($obj);
    }

    /**
     * get() / getAll() the sub parts of data
     * @param string|null $obj
     * @return bool
     */
    public function get($obj = null)
    {
        if (is_string($obj)) {
            $obj = $this->$obj;
        } else {
            foreach ($this->data as $key => $children) {
                $this->$key->get();
            }
            $obj = $this->data;
        }
        return $obj;
    }

    // TODO : check if this would work or need OBJ
    /**
     * set() the sub parts of data
     * @param $class
     * @param null|CollectionInterface $obj
     * @return bool
     */
    public function set($class, CollectionInterface $obj = null)
    {
        // set the new class to null
        if (is_string($class)) {
            $this->$class = null;
        }
        if (is_object($obj)) {
            $this->$class = $obj;
        }
        return $this->$class;
    }

    /**
     * @return array
     */
    public function getOpportunitiesStatic()
    {
        // TODO: Implement getOpportunitiesStatic() method.
    }
}
/* /SlimInsight */
