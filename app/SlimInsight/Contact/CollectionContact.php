<?php
/**
 * This file is part of the tristanbailey/sliminsight app
 *
 * CollectionContact Class
 * to look up and generate data from the Insightly api
 *
 * PHP version 5
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Tristan Bailey <tristanbailey@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://holdingbay.co.uk/projects/sliminsight/ Documentation
 * @link https://bitbucket.org/tristanbailey/sliminsight GitHub
 */

// TODO : clean down now abstracted

namespace Tristanbailey\SlimInsight\Contact;

use Tristanbailey\SlimInsight\CollectionInterface;

/**
 * Class CollectionContact
 * @property \Tristanbailey\SlimInsight\Contact\CollectionContact contacts
 * @property array data
 */
class CollectionContact implements \Iterator, CollectionInterface
{

    private $position = 0;
    private $iteration = 50;
    private $data = [];
    private $db;
    private $insightly;

    /**
     * @param \MongoDB $db
     * @param \Insightly\Insightly $insightly
     * @throws \Exception
     */
    public function __construct(\MongoDB $db, \Insightly\Insightly $insightly)
    {
        $this->position = 0;
        $this->data = []; // contacts
        $this->db = $db;
        $this->insightly = $insightly;

        if (!isset($this->db) && !isset($this->insightly)) {
            // TODO : make this better or remove
            throw new \Exception('Could not set up SlimContact');
        }

    }

    /**
     * magic method to allow (string) $object to to return something not fail
     * @return string
     */
    public function __toString()
    {
        return json_encode($this);
    }

    /**
     * Getter
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        if ($key === 'data') {
            return $this->data;
        }
        return $this->data[$key];
    }

    /**
     * Setter
     * @param $key
     * @param $value
     * @return mixed
     */
    public function __set($key, $value)
    {
        $this->data[$key] = $value;
        return $this->data[$key];
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->contacts[$this->position];
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return integer scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->contacts[$this->position]);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return $this->data;
        // TODO : should json encode return more data?
    }

    /**
     * Alias for built in
     * @param bool|int $contact_id
     * @return array
     */
    public function get($contact_id = false)
    {
        $output = $this->getContacts($contact_id);
        return $output;
    }

    /**
     * Return an $output of all on 1 contact from the DB
     * Should pass in an int but force that from the outside? Or force correct? << is hard to get all and 1 if Type here
     * @param bool|int $contact_id
     * @var array $data
     * @return array
     */
    public function getContacts($contact_id = false)
    {
        $data = [];
//        $dataCleaned = [];
        // select your collection
        // if its a non number then return a fail
        // A not false + B less 0 OR + is not int
        if ($contact_id !== false && ($contact_id < 0 || is_int($contact_id) === false)) {
            return $this->output($data, 400);
        }
        /**
         * @var \MongoCollection $collection
         */
        $collection = $this->db->contacts;

        // find one contact
        if ($contact_id !== false) {
            $contact_id = (int) $contact_id;
            /**
             * @var \MongoCursor $cursor
             */
            $cursor = $collection->find(['CONTACT_ID' => $contact_id]);
        } else {
            // sort updated first
            // find everything in the collection
            /**
             * @var \MongoCursor $cursor
             */
            $cursor = $collection->find()->sort(['DATE_UPDATED_UTC' => -1]);
        }

        /**
         * @var \MongoCursor $data
         */
        $data = iterator_to_array($cursor);
        $this->contacts = $data;
//        foreach ($data as $key => $document) {
//            // TODO : if there is no data in iterator then data will not get set // maybe set fore to this->data?
//            // reset the keys to ids so can be iterated
//            $dataCleaned[] = $document;
//        }
//        $this->contacts = $dataCleaned;

        /**
         * @var array $output
         */
//        $output = $this->output($this->contacts);
//        return $output;
        // TODO :  should get() return data or just fetch it? // get = return vs fetch is load.

        return true;

    }

    /**
     * Got to the api and get new data
     * Save data to local nosql
     * requires db and insightly
     * @return array|\Exception
     */
    public function refresh()
    {
//        $data = [];
        /**
         * @var \MongoDB $mongo
         */
        $mongo = $this->db;
        /**
         * get insightly data api
         * @var \Insightly\Insightly $i
         */
        $i = $this->insightly;

        // call api and get results
        $contacts = $i->getContacts();

        // TODO : if is false // if no data dont save.

        // connect
        // TODO : check there is collection and can save
        if ($mongo->selectCollection('contacts') === false) {
            return \Exception('Mongo Collection Missing: contacts', \Slim\Log::CRITICAL);
        }

        // TODO : handle \MongoCursorTimeoutException and \MongoCursorException round all db calls
        // drop old data from db and obj
        $mongo->contacts->remove();
        $this->contacts = [];
        /**
         * pass cursor over for use
         * @var \MongoCollection $collection
         */
        $collection = $mongo->contacts;

        // put data in mongo
        if (isset($contacts)) {
            $collection->batchInsert((array) $contacts);
        } else {
            return false;
        }

        // Now pull data back out of db and put in obj
        /**
         * put mongo data in obj
         * @var \MongoCollection $collection
         */
        $cursor = $collection->find();
        $this->contacts = iterator_to_array($cursor);

        /*
        foreach ($this->contacts as $doc) {
            // save a record of ids for output
            $data[] = $doc->CONTACT_ID;
        }
        */
        //var_dump($data);
                /**
                 * @var array $output
                 */
        /*
        $output = $this->output($data);
        return $output;
        */
        return true;

    }

    /**
     * @param string $tag
     * @return string
     */
    public function getTag($tag)
    {
        $dataCleaned = [];
        $i = $this->insightly;

        // in api /v2.1/Contacts?tag=$tag
//        $i->getPipelineStages();
        $contacts = $i->getContacts(['tag' => $tag]);

        $data = (array) $contacts;

        // TODO : should remove the clean --> output
        foreach ($data as $key => $document) {
            // TODO : if there is no data in iterator then data will not get set // maybe set fore to this->data?
            // reset the keys to ids so can be iterated
            $dataCleaned[] = $document;
        }
        // could story on Tag or var with Tag?
        $this->contacts = $dataCleaned;

        /**
         * @var array $output
         */
        $output = $this->output();
        return $output;
    }

    //TODO : could drop all params and just get from obj by default
    //TODO : could drop getter and just use this?
    /**
     * Prep the output array, same for all formats
     * @param array $data
     * @param int $response_code
     * @var int $response_code
     * @var array $output
     * @var int $response_code
     * @var int $count
     * @return array
     */
    public function output($data = null, $response_code = null)
    {
        // TODO : output should paginate return data
        // TODO : should data test and "return false" to fail output // but api would just be 404
        // if pass no data then use the obj
        if ($data === null) {
            $data = $this->contacts;
        }
        if (!is_array($data)) {
            $data = [];
        }
        $dataCleaned = [];
        $output = [];

        $response_code = ($response_code !== null || count($data) >= 1) ? 200 : 400;

        $count = count($data);

        foreach ($data as $doc) {
            // reset the ids
            $dataCleaned[] = $doc;
        }
        $data = $dataCleaned;

        /**
         * @var array $output
         */
        $output = [
            'response' => $response_code,
            'route' => '',
            'count' => $count,
            'data' => $data
        ];
        return $output;
    }
}
/* /CollectionContact */
