<?php
/**
 * FakeCollectionContact Class
 * change the data for CollectionContact Class
 *
 * PHP version 5
 *
 * @category Tristanbailey\SlimInsight
 * @package  Contact
 * @author   Tristan Bailey <tristanbailey@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @version  GIT: <git_id>
 * @link     http://holdingbay.co.uk
 */

// DONE : finish set up fake
// TODO : Should it just be output that styles fake?
// TODO : #Phase2 make possible to fake all the data and not use parent.

namespace Tristanbailey\SlimInsight\Contact;

use Faker\Generator as Faker;

/**
 * Class FakeCollectionContact
 * @property \Tristanbailey\SlimInsight\Contact\CollectionContact contacts
 */
class FakeCollectionContact extends CollectionContact
{
    /**
     * @var Faker
     */
    private $faker;
    /**
     * @var bool
     */
    private $isFake = true;
    /**
     * @var array
     */
    private $data = [];

    /**
     * @param \MongoDB $db
     * @param \Insightly\Insightly $insightly
     * @param Faker $faker
     * @param bool $isFake
     * @throws \Exception
     */
    public function __construct(\MongoDB $db, \Insightly\Insightly $insightly, Faker $faker, $isFake = true)
    {
        // need db and insightly for the parent
        parent::__construct($db, $insightly);
        $this->isFake = $isFake;

        // TODO : do not need to test if fake, as will be by default // Can you set it to not fake pass through?
        // need faker for this
//        if ($isFake === true && is_object($faker) && get_class($faker) === 'Faker\Generator') {
        if (is_object($faker) && get_class($faker) === 'Faker\Generator') {
//            $this->isFake = $isFake;
            $this->faker = $faker;
        }

        // TODO  : might not reach so not needed?
        if (is_null($this->faker)) {
            throw \Exception('Object requires Faker');
        }

    }

    /**
     * Return an $output of all on 1 contact from the DB
     * Should pass in an int but force that from the outside? Or force correct? << is hard to get all and 1 if Type here
     * @param bool|int $contact_id
     * @return array
     */
    public function get($contact_id = false)
    {
        /**
         * @var array $data
         */
        $data = [];
        /**
         * @var array $dataClean
         */
        $dataClean = [];
        try {
            $data = parent::get();
        } catch (\Exception $e) {
            echo $e->getMessage() . ' ' . $e->getFile();
        }
        $data = $data['data'];

        $this->contacts = $data;
//        foreach ($data as $key => $document) {
//            // overwrite fake data
//            $document['FIRST_NAME'] = $this->faker->firstName;
//            $document['LAST_NAME'] = $this->faker->lastName;
//            $document['DATE_CREATED_UTC'] = $this->faker->dateTimeBetween('-1 years', '+9 months')->format('Y-m-d H:i:s');
//            $document['BACKGROUND'] = $this->faker->realText(100);
//            if (is_array($document['CONTACTINFOS']) && isset($document['CONTACTINFOS'][0]) && $document['CONTACTINFOS'][0]['TYPE'] === 'EMAIL') {
//                $document['CONTACTINFOS'][0]['DETAIL'] = $this->faker->companyEmail;
//            }
//            // TODO : if there is no data in iterator then data will not get set // maybe set fore to this->data?
//            // reset the keys to ids so can be iterated
//            $dataCleaned[] = $document;
//        }
//        $this->contacts = $dataCleaned;

        /**
         * @var array $output
         */
//        $output = $this->output($this->contacts);
//        return $output;
        return true;

    }

    /**
     * @param string $tag
     * @return bool|string
     */
    public function getTag($tag)
    {
        $dataCleaned = [];

        parent::getTag($tag);
//        $data = $this->contacts;
//
//        $this->contacts = $data;
//        foreach ($data as $key => $document) {
//            $document->FIRST_NAME = $this->faker->firstName;
//            $document->LAST_NAME = $this->faker->lastName;
//            $document->DATE_CREATED_UTC = $this->faker->dateTimeBetween('-1 years', '+9 months')->format('Y-m-d H:i:s');
//            $document->BACKGROUND = $this->faker->realText(100);
//            if (is_array($document->CONTACTINFOS) && isset($document->CONTACTINFOS[0]) && $document->CONTACTINFOS[0]->TYPE === 'EMAIL') {
//                $document->CONTACTINFOS[0]->DETAIL = $this->faker->companyEmail;
//            }
//            // reset the keys to ids so can be iterated
//            $dataCleaned[] = $document;
//        }
//        $this->contacts = $dataCleaned;
        /**
         * @var array $output
         */
//        $output = $this->output($this->contacts);
//        return $output;
        return true;
    }

    public function output($data = null, $response_code = null)
    {
        $dataCleaned = [];
        $output = parent::output($data, $response_code);

        foreach ($data as $key => $document) {
            $document->FIRST_NAME = $this->faker->firstName;
            $document->LAST_NAME = $this->faker->lastName;
            $document->DATE_CREATED_UTC = $this->faker->dateTimeBetween('-1 years', '+9 months')->format('Y-m-d H:i:s');
            $document->BACKGROUND = $this->faker->realText(100);
            if (is_array($document->CONTACTINFOS) && isset($document->CONTACTINFOS[0]) && $document->CONTACTINFOS[0]->TYPE === 'EMAIL') {
                $document->CONTACTINFOS[0]->DETAIL = $this->faker->companyEmail;
            }
            // reset the keys to ids so can be iterated
            $dataCleaned[] = $document;
        }
        $output['data'] = $dataCleaned;

        return $output;
    }
}
/* /FakeCollectionContact */
