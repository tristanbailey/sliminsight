<?php
/**
 * This file is part of the tristanbailey/sliminsight app
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @copyright Copyright (c) Tristan Bailey <tristanbailey@gmail.com>
 * @license http://opensource.org/licenses/MIT MIT
 * @link https://holdingbay.co.uk/projects/sliminsight/ Documentation
 * @link https://bitbucket.org/tristanbailey/sliminsight GitHub
 */

namespace Tristanbailey\SlimInsight;


interface CollectionInterface
{

    /**
     * @return array
     */
    public function get($contact_id = false);

    /**
     * @return array
     */
    public function refresh();

    /**
     * @param $data
     * @param $response_code
     * @return array
     */
    public function output($data, $response_code = null);

}
