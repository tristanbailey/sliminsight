<?php
/**
 * Dev server settings
 * User: tristanbailey
 * Date: 03/08/2015
 * Time: 01:59
 */

return [
    'app' => [
        'url' => $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'], //'http://localhost:8888',
        'loc' => '/sliminsight/public',
        'hash' => [
            'algo' => PASSWORD_BCRYPT,
            'cost' => 10
        ],
        'cookies' => [
            'secret_key' => 'KEY',
        ],
        'templates' => [
            'path' => '../app/templates/',
        ],
        'version' => '0.7.5',
        'fake' => false,
    ],
    'auth' => [
        'session' => 'user_id',
        'remember' => 'user_r'
    ],
    'twig' => [
        'debug' => true,
        'path' => '../app/views',
    ],
    'csrf' => [
        'session' => 'csrf_token'
    ],
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'name' => 'slim',
        'username' => 'USER',
        'password' => 'PASSWORD',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => ''
    ],
    'mongo' => [
        'driver' => 'mongo',
        'host' => 'localhost',
        'name' => 'NAME',
        'db' => 'DB',
    ],
    'mail' => [
    ],
    'insightly' => [
        'key' => 'KEY',
        'domain' => 'https://XYZ.insight.ly/',
    ],
    'connectio' => [
        'key' => 'KEY',
        'domain' => 'DOMAIN',
    ],

];

