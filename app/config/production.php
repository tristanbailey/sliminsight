<?php
/**
 * Prod server settings
 * User: tristanbailey
 * Date: 03/08/2015
 * Time: 01:59
 */

return [
    'app' => [
        'url' => $_SERVER['SERVER_NAME'], //'http://it.holdingbay.co.uk',
        'loc' => '',
        'hash' => [
            'algo' => PASSWORD_BCRYPT,
            'cost' => 10
        ],
        'cookies' => [
            'secret_key' => 'dc9-Vdsnj;22',
        ],
        'templates' => [
            'path' => '../app/templates/',
        ],
        'version' => '0.7.0',
        'fake' => true,
    ],
    'auth' => [
        'session' => 'user_id',
        'remember' => 'user_r',
    ],
    'twig' => [
        'debug' => true,
        'path' => '../app/views',
    ],
    'csrf' => [
        'session' => 'csrf_token',
    ],
    'db' => [
        'driver' => 'mysql',
        'host' => 'localhost',
        'name' => 'inspire_training',
        'username' => 'inspire',
        'password' => 'MUAfYS58Nb5MHVmK',
        'charset' => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix' => 'oct_',
    ],
    'mongo' => [
        'driver' => 'mongo',
        'host' => 'localhost',
        'name' => 'sliminsight',
        'db' => 'sliminsight',
    ],
    'mail' => [
    ],
    'insightly' => [
        'key' => 'a688c402-3238-42b4-b456-7200872c8071',
        'domain' => 'https://holdingbay.insight.ly/',
    ],
    'connectio' => [
        'key' => 'F163E656029D1D9D9C4CC351B89211DD-841A889AFDBF7C577E67A901D54B4ADE2C170124AB2F660A7A8BC3B706EF0041E9E28E71642DBA73AD7A05B235537EEA3794F3F2720F82EBDE1DD12B50B02F34',
        'domain' => '55991e991f2ddd12e4427ea7',
    ],

];

