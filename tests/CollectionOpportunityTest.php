<?php
/**
 * Created by PhpStorm.
 * User: tristanbailey
 * Date: 16/08/2015
 * Time: 16:14
 */

namespace Tristanbailey\Sliminsight\Opportunity\Test;

use Tristanbailey\SlimInsight\Opportunity\CollectionOpportunity;
use Tristanbailey\SlimInsight\CollectionInterface;

class CollectionOpportunityTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CollectionOpportunity
     */
    protected $collection;
    protected static $db;
    protected static $insightly;

    public static function setUpBeforeClass()
    {
        $m = new \MongoClient();
        self::$db = $m->selectDB('sliminsight');
        self::$insightly = new \Insightly\Insightly('a688c402-3238-42b4-b456-7200872c8071');
    }

    public static function tearDownAfterClass()
    {
        self::$db = NULL;
        self::$insightly = NULL;
    }

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->collection = new CollectionOpportunity(self::$db, self::$insightly);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }

    /**
     * @expectedExceptionMessage Not CollectionOpportunity
     */
    public function testOpportunityIsCollectionOpportunity()
    {
        $actual = $this->collection;
        $this->assertInstanceOf('Tristanbailey\SlimInsight\CollectionInterface', $actual);
        $this->assertInstanceOf('Tristanbailey\SlimInsight\Opportunity\CollectionOpportunity', $actual, 'Not CollectionOpportunity');
    }

    /**
     * @covers                   Tristanbailey\SlimInsight\Opportunity\CollectionOpportunity::_construct()
     * @covers                   Tristanbailey\SlimInsight\Opportunity\CollectionOpportunity::data
     */
    public function testOpportunityIsEmptyArray()
    {
        $this->assertEmpty($this->collection->data, 'Not empty array');
    }
}
