# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased][unreleased]
### Changed
- there is now a login
- Moved functions to their own classes
- REFACTOR boot"Class" DI to use annon functions in $container

## [0.7.2] - 2015-08-09
### Added
- /get/api/contacts/:contact_id to get public contact 

### Changed
- CLEAN UP config to now use app->config->get('x') for all outside slim
- CLeaned $app->server but should now use it 
- API should show error code but not error page
- Refresh methods now in SlimInsight
- Stage names now match real names and come from api
- CHANGED colour now comes a different way because of the new key in tpl

### Fixed
- REFACTOR bootMongo so if no db will create one

## [0.7.1] - 2015-08-02
### Added
- use of Staging from the saas

### Changed
- Moved to the obj the get data refresh methods, also batch insert now

### Fixed
- Some typos and possible unset calls.
