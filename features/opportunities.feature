Feature: Web Contacts
    In order to see Contacts
    As a web user
    I need to be able to list all the Contacts

    @core
    Scenario: Visit contacts
        Given I am on "/opportunities"
        When I follow "Contacts"
        Then I should see "Contacts in the House"

    Scenario: Contacts from DB
        When I check database "opportunities" with "get"
        Then I check database "opportunities" with "get"

    
