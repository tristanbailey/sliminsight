Feature: Web opportunities
    In order to see opportunities
    As a web user
    I need to be able to list all the opportunities

    @core
    Scenario: Visit Opportunities stages
        Given I am on "/opportunities"
        When I follow "Opportunities"
        Then I should see "Opportunities jump around"

    Scenario: Visit Opportunities to see Development tasks
      Given I am on "/opportunities"
      When I follow "Opportunities"
      Then I should see "Development"

    @core
    Scenario: Visit Opportunities Months
      Given I am on "/opportunities"
      When I follow "Months"
      Then I should see "Months in the House"

    Scenario: Visit Opportunities Months to see this next month
      Given I am on "/opportunities"
      When I follow "Months"
      Then I should see "September"

    Scenario: Visit Opportunities Months to see three months
      Given I am on "/opportunities"
      When I follow "Months"
      Then I should see 4 "h2" elements
    
