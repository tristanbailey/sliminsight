Feature: Rely on external calls
    In order to run this system
    As a server
    I need to be able to rely on these services

    Scenario: MongoDB is running 
        When I call "MongoClient"
        Then I should see Connection
 
    Scenario: MongoDB has data
        When I call "MongoClient"
        And I request data
        Then I should see Data

    Scenario: Insightly api present
        When I call "InsightlyClient"
        Then I should see Connection

    Scenario: Insightly api returns opp
        When I call "Insightly" for "getOpportunities"
        Then I should see "count" greater than "10"

    Scenario: List 3874681 as 1 opportunity from the api
        Given I am on "X/api/v1/opportunities"
        When I am on "/api/v1/opportunities/3874681"
        Then I should GET "/api/v1/opportunities/3874681" and see JSON:
"""
{"response":200,"route":"it.holdingbay.co.uk\/get\/opportunities","count":1,"data":{"5578d16d779217b1178b45cc":{"_id":{"$id":"5578d16d779217b1178b45cc"},"OPPORTUNITY_ID":3874681,"OPPORTUNITY_NAME":"TUi Travel Jobs site","OPPORTUNITY_DETAILS":null,"PROBABILITY":10,"BID_CURRENCY":"GBP","BID_AMOUNT":8000,"BID_TYPE":"Fixed Bid","BID_DURATION":null,"FORECAST_CLOSE_DATE":"2014-07-04 00:00:00","CATEGORY_ID":1985535,"PIPELINE_ID":96512,"STAGE_ID":294142,"OPPORTUNITY_STATE":"ABANDONED","IMAGE_URL":"http:\/\/s3.amazonaws.com\/insightly.userfiles\/309678\/","RESPONSIBLE_USER_ID":560190,"OWNER_USER_ID":560190,"DATE_CREATED_UTC":"2014-07-04 01:51:00","DATE_UPDATED_UTC":"2014-09-12 09:05:38","VISIBLE_TO":"EVERYONE","VISIBLE_TEAM_ID":null,"VISIBLE_USER_IDS":null,"CUSTOMFIELDS":[],"TAGS":[],"LINKS":[{"LINK_ID":43878223,"CONTACT_ID":67301745,"OPPORTUNITY_ID":3874681,"ORGANISATION_ID":null,"PROJECT_ID":null,"SECOND_PROJECT_ID":null,"SECOND_OPPORTUNITY_ID":null,"ROLE":null,"DETAILS":null}],"EMAILLINKS":[]}}}
"""
