Feature: api contacts
    In order to see contacts
    As a web user
    I need to be able to list all the contacts

    @core
    Scenario: List all contacts from the api
        Given I am on "/api/v1/contacts"
        #When I send a GET request to "/api/v1/contacts"
        Then I should GET "/api/v1/contacts" and see JSON "count" greater than "10"

    Scenario: List one contacts from the api
        Given I am on "/api/v1/contacts/123214215"
        Then I should GET "/api/v1/contacts/123214215" and see JSON "count" is "1"

    Scenario: List 124234756 as 1 contact from the api
        Given I am on "/api/v1/contacts"
        When I am on "/api/v1/contacts/124234756"
        Then I should GET "/api/v1/contacts/123214215" and see JSON:
"""
{"response":200,"route":"\/api\/v1\/contacts\/123214215","count":1,"data":{"55757ae8779217e0508e63d2":{"_id":{"$id":"55757ae8779217e0508e63d2"},"CONTACT_ID":124234756,"SALUTATION":null,"FIRST_NAME":"Adam","LAST_NAME":"Smith","BACKGROUND":"does business planning and stats and connected on linkedin \r\n\r\nsmudgersmith74@hotmail.co.uk (SKYPE)\r\n\r\nhttps:\/\/uk.linkedin.com\/pub\/adam-smith\/83\/248\/219","IMAGE_URL":"http:\/\/s3.amazonaws.com\/insightly.userfiles\/309678\/Q1DSBX\/cbf1fa6d-d281-42fa-a03a-6950cd60dec4.jpeg","DEFAULT_LINKED_ORGANISATION":59290409,"OWNER_USER_ID":560190,"DATE_CREATED_UTC":"2015-06-08 10:50:35","DATE_UPDATED_UTC":"2015-06-08 10:53:53","VISIBLE_TO":"EVERYONE","VISIBLE_TEAM_ID":null,"VISIBLE_USER_IDS":null,"CUSTOMFIELDS":[],"ADDRESSES":[],"CONTACTINFOS":[{"CONTACT_INFO_ID":214441761,"TYPE":"EMAIL","SUBTYPE":null,"LABEL":"WORK","DETAIL":"Adam.Smith@wild.net"},{"CONTACT_INFO_ID":214441762,"TYPE":"EMAIL","SUBTYPE":null,"LABEL":"WORK","DETAIL":"smudgersmith74@hotmail.co.uk"},{"CONTACT_INFO_ID":214441763,"TYPE":"PHONE","SUBTYPE":null,"LABEL":"WORK","DETAIL":"07572456310"}],"DATES":[],"TAGS":[{"TAG_NAME":"bizdev"},{"TAG_NAME":"linkedin"},{"TAG_NAME":"New Leads"},{"TAG_NAME":"website"}],"LINKS":[{"LINK_ID":77030054,"CONTACT_ID":124234756,"OPPORTUNITY_ID":null,"ORGANISATION_ID":59290409,"PROJECT_ID":null,"SECOND_PROJECT_ID":null,"SECOND_OPPORTUNITY_ID":null,"ROLE":"Support Engineer","DETAILS":null}],"CONTACTLINKS":[],"EMAILLINKS":[]}}}
"""

        Scenario: Refresh data in the obj
            Given I am on "/api/v1/"
            When I am visit "/api/v1/refresh"
            Then I should GET "/api/v1/refresh" and see JSON:
            """
            {"response":404,"route":"\/v1\/api\/refresh","count":0,"data":[]}
            """
##           {"response":200,"route":"\/v1\/api\/refresh","count":2,"data":{}}
