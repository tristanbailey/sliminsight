<?php
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Context\BehatContext;
#   Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode,
    Behat\Gherkin\Node\TableNode;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\SnippetAcceptingContext;

use Insightly\Insightly;
use Tristanbailey\SlimInsight\SlimInsight;

require_once 'app/Insightly/Insightly.php';



/**
** Features context.
**/
class FeatureContext extends MinkContext implements SnippetAcceptingContext
{
    public $server = 'http://it.holdingbay.co.uk';
    //public $server = 'http://localhost:8888/sliminsight/public';
    public $key = 'a688c402-3238-42b4-b456-7200872c8071'; #TODO : pull key out to settings
    public $timeout = '4.0';

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {

        // connect MongoDB
        $m = new MongoClient();
        $db = $m->sliminsight; // db

        // connect Insightly
        $insightly = false;
        try {
            if (class_exists('\Insightly\Insightly'))
            {
                $insightly = new Insightly($this->key);
            }
            else
            {
                throw new \Exception('Missing Insightly Class');
            }
        } catch (\Exception $e) {

            echo 'ERROR' . $e->getMessage() . ' ' . $e->getFile() . ' ' . $e->getLine();// var_dump($e); #TODO : what do here with error?
        }

        $slimInsight = new SlimInsight($db, $insightly);


        $this->obj = $slimInsight;
    }


    /**
     * @Then I should GET :arg1 and see JSON:
     */
    public function iShouldGetAndSeeJson($arg1, PyStringNode $string)
    {
        $client = new GuzzleHttp\Client([
            // Base URI is used with relative requests
            'base_uri' => $this->server,
            // You can set any number of default request options.
            'timeout'  => $this->timeout,
        ]);
        $response = $client->get($arg1);
        $get = (string) $response->getBody();
        #echo $get;

        if (strpos($get, (string) $string) )
        {
            throw new Exception('No path data');
        }
    }
    
    /**
     * @Then I should GET :arg1 and see JSON :arg2 greater than :arg3
     */
    public function iShouldGetAndSeeJsonGreaterThan($arg1, $arg2, $arg3)
    {
        echo 'try' . $this->server . $arg1;
        $client = new GuzzleHttp\Client([
            'base_uri' => $this->server,
            'timeout'  => $this->timeout,
        ]);
        $response = $client->get($arg1);
//        echo $response;
        $get = (string) $response->getBody();
        $get = json_decode($get);
//        echo $get;
//        echo $get->$arg2;
//        var_dump($get->$arg1);
        var_dump($get->$arg2);
//        var_dump($get);
        #var_dump((int) $arg3);

        // use the args as keys to the get obj
        if (isset($get->$arg2) && $get->$arg2 <= (int) $arg3 )
        {
        throw new Exception('Count not greater');
        }
    }

    /**
     * @Then I should GET :arg1 and see JSON :arg2 is :arg3
     */
    public function iShouldGetAndSeeJsonIs($arg1, $arg2, $arg3)
    {
        $client = new GuzzleHttp\Client([
            'base_uri' => $this->server,
            'timeout'  => $this->timeout,
        ]);
        $response = $client->get($arg1);
        $get = (string) $response->getBody();
        $get = json_decode($get);
        #echo $get->$arg2;
        var_dump($get->$arg2);
        #var_dump((int) $arg3);

        if (isset($get->$arg2) && $get->$arg2 != (int) $arg3 )
        {
        throw new Exception('Count not equal');
        }
    }

    /**
     * @When I send a GET request to :arg1
     */
    public function iSendAGetRequestTo($arg1)
    {
        $client = new GuzzleHttp\Client([
            'base_uri' => $this->server,
            'timeout'  => $this->timeout,
        ]);
        $response = $client->get($arg1);
        $get = (string) $response->getBody();
        $get = json_decode($get);
        #echo $get;

        if ($get->response != 200 )
        {
        throw new Exception('No path');
        }
    }

    /**
     * @Then I should get :arg1 greater than :arg2
     */
    public function iShouldGetGreaterThan($arg1, $arg2)
    {
        throw new PendingException();
    }

    /**
     * @When I check database :arg1
     */
    public function iCheckDatabase($arg1)
    {
        $this->obj->$arg1(); // not inside $app so db out I think
    }
}
