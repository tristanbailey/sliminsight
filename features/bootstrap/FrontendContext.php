<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

#echo 'hello';
#exit();
/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context, SnippetAcceptingContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
echo 'hello';
    }


    /**
     * @Given I am at path :arg1
     */
    public function iAmAtPath($arg1)
    {
	$client = new GuzzleHttp\Client([
		// Base URI is used with relative requests
		'base_uri' => 'http://it.holdingbay.co.uk',
		// You can set any number of default request options.
		'timeout'  => 2.0,
	]);
	$response = $client->get($arg1);
	$get = (string) $response->getBody();
#	echo $get;
	if (strpos($get, 404) !== false)
	{
        throw new Exception('No path');
	}
    }

	    


    /**
     * @When I run :arg1
     */
    public function iRun($arg1)
    {
	$client = new GuzzleHttp\Client([
		// Base URI is used with relative requests
		'base_uri' => 'http://it.holdingbay.co.uk',
		// You can set any number of default request options.
		'timeout'  => 2.0,
	]);
	$response = $client->get($arg1);
	$get = (string) $response->getBody();
#	echo $get;

	if (strpos($get, 404) !== false)
	{
        throw new Exception('No path');
	}
    }

    /**
     * @Then I should get:
     */
    public function iShouldGet(PyStringNode $string)
    {
	$client = new GuzzleHttp\Client([
		// Base URI is used with relative requests
		'base_uri' => 'http://it.holdingbay.co.uk',
		// You can set any number of default request options.
		'timeout'  => 2.0,
	]);
	$response = $client->get('/get/contacts/124234756');
	$get = (string) $response->getBody();
	#echo $get;

	if (strpos($get, (string) $string) )
	{
        throw new Exception('No path');
	}
    }


    /**
    ** @When I run count :arg1
    **/
    public function iRunCount($arg1)
    {
    }

    /**
    ** @Then I should get :arg1 more than:
    **/
    public function iShouldGetMoreThan($arg1, PyStringNode $string)
    {
    }

    /**
     * @When I run count :arg1
     */
    public function iRunCount3($arg1)
    {
	$client = new GuzzleHttp\Client([
		'base_uri' => 'http://it.holdingbay.co.uk',
		'timeout'  => 2.0,
	]);
	$response = $client->get($arg1);
	$get = (string) $response->getBody();
	$get = json_decode($get);
	#echo $get;

	if ($get->count == 0 )
	{
	throw new Exception('No path');
	}
    }


    /**
     * @Then I should get :arg1 more than :arg2
     */
    public function iShouldGetMoreThan2($arg1, $arg2)
    {
	$client = new GuzzleHttp\Client([
		'base_uri' => 'http://it.holdingbay.co.uk',
		'timeout'  => 2.0,
	]);
	$response = $client->get($arg1);
	$get = (string) $response->getBody();
	$get = json_decode($get);
	//print_r($get);

	if ($get->count < $arg2 )
	{
	throw new Exception('No path');
	}
    }

}
